#!/system/bin/sh
LOGDISK=$(getprop persist.sys.lenovo.log.disk)
#LOGFOLDER=$(getprop persist.sys.lenovo.log.folder)
OUTFOLDER=$LOGDISK"/log_out"
SAVELOG_SHELL="/system/bin/savelog.sh"

FILENAME=$(date +%Y_%m_%d_%H_%M_%S)
OUTFILE=$OUTFOLDER/${FILENAME}.tgz
setprop persist.sys.lenovo.log.outfile $OUTFILE

mkdir -p $OUTFOLDER
cd $LOGDISK
FILES_LOG=`ls -r log | sed 's/\n//' | sed 's/^/log\//'`
FILES_DIAGLOG=`ls -r diag_logs | sed 's/\n//' | sed 's/^/diag_logs\//'`
tar zcf $OUTFILE  $FILES_LOG $FILES_DIAGLOG

cd $LOGDISK && chown -R media_rw:media_rw log_out

if [ $(getprop persist.sys.lenovo.log.save) = CLEAN ]; then
     $SAVELOG_SHELL CLEAN
fi


/system/bin/cmd activity broadcast -a android.lenovo.action.COMPRESS_LENOVO_LOG_DONE -p com.lenovo.loggerpannel --es path $OUTFOLDER --es path_result $OUTFILE

