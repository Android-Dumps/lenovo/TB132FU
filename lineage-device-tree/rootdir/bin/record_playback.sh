#! /system/bin/sh
#echo "Playback test"
spk1=1
spk2=2
spk3=3
spk4=4
mic1_record=5
mic2_record=6
enable=1
disable=0
open="-Y"
close="-N"
pname_play="tinyplay"
pbname="loopbacktest"

if test $2 -eq $enable
then
	loopbacktest $open "$1" 13
	if test $1 -eq $spk1
	then
		echo 0004820  00020102 >d/regmap/6-0040/registers	
	elif test $1 -eq $spk2
	then
		echo 0004820  00020102 >d/regmap/6-0041/registers
	elif test $1 -eq $spk3
	then
		echo 0004820  00020102 >d/regmap/3-0042/registers
	elif test $1 -eq $spk4
	then
		echo 0004820  00020102 >d/regmap/3-0043/registers
	fi
		tinyplay sdcard/miccapture_4ch.wav -D 0 -d 21
		pkill -f $pname_play
		loopbacktest $close "$1"
		echo 0004820  00020100 >d/regmap/6-0040/registers
		echo 0004820  00020100 >d/regmap/6-0041/registers
		echo 0004820  00020100 >d/regmap/3-0042/registers
		echo 0004820  00020100 >d/regmap/3-0043/registers		
		# end close speaker playback
elif test $2 -eq $disable
then
	if test $1 -eq $spk1 -o $1 -eq $spk2 -o $1 -eq $spk3 -o $1 -eq $spk4
	then
	#pkill -f $pbname
	echo 0004820  00020100 >d/regmap/6-0040/registers
	echo 0004820  00020100 >d/regmap/6-0041/registers
	echo 0004820  00020100 >d/regmap/3-0042/registers
	echo 0004820  00020100 >d/regmap/3-0043/registers		
	pkill -f $pname_play
	loopbacktest $close "$1"
	else
	loopbacktest $close "$1"
	fi
else
	echo "input error ctl cmd!"
fi
