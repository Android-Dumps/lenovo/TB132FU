#! /system/bin/sh
#echo "record test"
spk1=1
spk2=2
spk3=3
spk4=4
mic1_record=5
mic2_record=6
mic1_4ch_record=7
mic2_4ch_record=8
adc_record=9
enable=1
disable=0
open="-Y"
close="-N"
pname_rec="tinycap"
pname_play="tinyplay"
pbname="loopbacktest"


if test $2 -eq $enable
then
	loopbacktest $open "$1" 13
	if test $1 -eq $mic1_record -o $1 -eq $mic2_record
	then
		tinycap /sdcard/miccapture.wav -D 0 -d 10 -c 1 -r 48000 -b 16 -T $3
		pkill -f $pname_rec
		loopbacktest $close "$1"
	elif test $1 -eq $mic1_4ch_record -o $1 -eq $mic2_4ch_record
	then
		tinycap /sdcard/miccapture_4ch.wav -D 0 -d 10 -c 4 -r 48000 -b 16 -T $3
		pkill -f $pname_rec
		loopbacktest $close "$1"
	elif test $1 -eq $adc_record
	then
		tinycap /sdcard/adc_rec.wav -D 0 -d 11 -c 2 -r 48000 -b 16 -T 5 | tinyplay vendor/etc/spk_pb.wav -D 0 -d 21
		pkill -f $pname_play
		pkill -f $pname_rec
		loopbacktest $close "$1"
	fi
elif test $2 -eq $disable
then
	if test $1 -eq $mic1_record -o $1 -eq $mic2_record -o $1 -eq $mic1_4ch_record -o $1 -eq $mic2_4ch_record
	then
	pkill -f $pname_rec
	loopbacktest $close "$1"
	fi
else
	echo "input error ctl cmd!"
fi
