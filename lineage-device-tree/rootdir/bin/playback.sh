#! /system/bin/sh
#echo "Playback test"
spk1=1
spk2=2
spk3=3
spk4=4
enable=1
disable=0
open="-Y"
close="-N"
pname_play="tinyplay"
pbname="loopbacktest"

if test $2 -eq $enable
then
	loopbacktest $open "$1" 13
	if test $1 -eq $spk1 -o $1 -eq $spk2 -o $1 -eq $spk3 -o $1 -eq $spk4
	then
		tinyplay /vendor/etc/spk_pb.wav -D 0 -d 21
		pkill -f $pname_play
		loopbacktest $close "$1"
	fi
elif test $2 -eq $disable
then
	if test $1 -eq $spk1 -o $1 -eq $spk2 -o $1 -eq $spk3 -o $1 -eq $spk4
	then
	pkill -f $pname_play
	loopbacktest $close "$1"
	fi
else
	echo "input error ctl cmd!"
fi
