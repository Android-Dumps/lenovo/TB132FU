#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from p11_pro_2gen device
$(call inherit-product, device/lenovo/p11_pro_2gen/device.mk)

PRODUCT_DEVICE := p11_pro_2gen
PRODUCT_NAME := lineage_p11_pro_2gen
PRODUCT_BRAND := Lenovo
PRODUCT_MODEL := TB132FU
PRODUCT_MANUFACTURER := lenovo

PRODUCT_GMS_CLIENTID_BASE := android-lenovo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="TB132FU-user 12 SP1A.210812.016 TB132FU_S000104_230329_ROW release-keys"

BUILD_FINGERPRINT := Lenovo/TB132FU/TB132FU:12/SP1A.210812.016/TB132FU_S000104_230329_ROW:user/release-keys
