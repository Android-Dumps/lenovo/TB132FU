#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_p11_pro_2gen.mk

COMMON_LUNCH_CHOICES := \
    lineage_p11_pro_2gen-user \
    lineage_p11_pro_2gen-userdebug \
    lineage_p11_pro_2gen-eng
