#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_p11_pro_2gen.mk

COMMON_LUNCH_CHOICES := \
    omni_p11_pro_2gen-user \
    omni_p11_pro_2gen-userdebug \
    omni_p11_pro_2gen-eng
